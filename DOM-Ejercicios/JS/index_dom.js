import hamburgerMenu from './menu_hamburguesa.js';
import { digitalClock, alarm } from './reloj.js';
import { moveBall, shortcuts } from './teclado.js';
import countdown from './cuenta_regresiva.js';
import scrollTopButton from './boton_scroll.js';
import darkTheme from './tema_oscuro.js';

const d = document;

d.addEventListener('DOMContentLoaded', (e) => {
  hamburgerMenu('.panel-btn', '.panel', '.menu a');
  digitalClock('#reloj', '#activar-reloj', '#desactivar-reloj');
  alarm('./../assets/alarma.mp3', '#activar-alarma', '#desactivar-alarma');
  countdown(
    'countdown',
    'jun 21,2021 08:15:19',
    'Feliz cumpleaños amigo autodidacta🤓'
  );
  scrollTopButton('.scroll-top-btn');
});

d.addEventListener('keydown', (e) => {
  shortcuts(e);
  moveBall(e, '.ball', '.stage');
});

darkTheme('.dark-theme-btn', 'dark-mode');
