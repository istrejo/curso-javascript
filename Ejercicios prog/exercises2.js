/* 5) Programa una función que invierta las palabras de una cadena de texto, pe. miFuncion("Hola Mundo") devolverá "odnuM aloH". */

const invertirCadena = (cadena = "") => {
  (!cadena)
    ? console.warn("No ingresaste la cedena de texto")
    : typeof cadena !== "string"
    ? console.error("El valor ingresado no es una cadena de texto")
    : console.info(cadena.split("").reverse().join(""));
};

// invertirTexto()
// invertirTexto(2)
// invertirTexto("Hola mundo");

/* 6) Programa una función para contar el número de veces que se repite una palabra en un texto largo, pe. miFuncion("hola mundo adios mundo", "mundo") devolverá 2. */

const textoEnCadena = (cadena = "", texto = "") => {
  if (!cadena) return console.warn("No ingresaste el texto largo");

  if (!texto) return console.warn("No ingresaste la palabra a evaluar");

  let i = 0,
    contador = 0;

  while (i !== -1) {
    i = cadena.indexOf(texto, i);
    if (i !== -1) {
      i++;
      contador++;
    }
  }
  return console.info(`La palabra "${texto}" se repite ${contador} veces `);
};

// textoEnCadena();
// textoEnCadena("","mundo");
// textoEnCadena("Hola mundo  adios mundo");
// textoEnCadena("Hola mundo chao mundo", "mundo");

/* 7) Programa una función que valide si una palabra o frase dada, es un palíndromo (que se lee igual en un sentido que en otro), pe. mifuncion("Salas") devolverá true. */

const palindromo = (palabra = "") => {
  if (!palabra) return console.warn("No ingresaste una palabra o frase");

  if (typeof palabra !== "string")
    return console.error("El valor ingresado no es una cadena de texto");

  palabra = palabra.toLowerCase();
  let invertida = palabra.split("").reverse().join("");

  return palabra === invertida
    ? console.info(`Sí es palíndromo, palabra origina "${palabra}", palabra al réves "${invertida}"`)
    : console.info(`No es palíndromo, palabra origina "${palabra}", palabra al réves "${invertida}"`);
};

palindromo();
palindromo("salas");
palindromo("Hola mundo");
palindromo("oro");

/* 8) Programa una función que elimine cierto patrón de caracteres de un texto dado, pe. miFuncion("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz") devolverá  "1, 2, 3, 4 y 5. */

const eliminarPatron = (texto = "", patron = "") =>
  (!texto)
    ? console.warn("No ingresaste un texto")
    : !patron
    ? console.warn("No ingresaste un patrón a eliminar")
    : console.info(texto.replace(new RegExp(patron, "ig"), ""));

eliminarPatron();
eliminarPatron("xyz1, xyz2, xyz3, xyz4 y xyz5");
eliminarPatron("xyz1, xyz2, xyz3, xyz4 y xyz5", "xyz");
