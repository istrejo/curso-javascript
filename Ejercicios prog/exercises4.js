/*12) Programa una función que determine si un número es primo (aquel que solo es divisible por sí mismo y 1) o no, pe. miFuncion(7) devolverá true.*/

const numeroPrimo = (numero = undefined) => {
  if (numero === undefined) return console.warn("No ingresaste un número");

  if (typeof numero !== "number")
    return console.error(`El valor "${numero}" ingresado no es un número`);

  if (numero === 0) return console.error("El número no puede ser 0");

  if (numero === 1) return console.error("El número no puede ser 1");

  if (Math.sign(numero) === -1)
    return console.error("El número no puede ser negativo");

  let divisible = false;

  for (let i = 2; i < numero; i++) {
    if (numero % i === 0) {
      divisible = true;
      break;
    }
  }

  return divisible
    ? console.info(`El número ${numero} no es primo`)
    : console.info(`El número ${numero} sí es primo`);
};

// numeroPrimo();
// numeroPrimo("320");
// numeroPrimo(true);
// numeroPrimo(0);
// numeroPrimo(1);
// numeroPrimo(-13);
// numeroPrimo(13);
// numeroPrimo(200);

/* 13) Programa una función que determine si un número es par o impar, pe. miFuncion(29) devolverá Impar.*/

const parImpar = (numero = undefined) => {
  if (numero === undefined) return console.warn("No ingresaste el número a evaluar");

  if (typeof numero !== "number") return console.error(`El valor "${numero}" ingresado no es un número`);

  return (numero % 2 === 0)
    ? console.info(`El número ${numero} es par`)
    : console.info(`El número ${numero} es impar`);
};

// parImpar();
// parImpar("22");
// parImpar(false);
// parImpar(-270);
// parImpar(19);

/* 14) Programa una función para convertir grados Celsius a Fahrenheit y viceversa, pe. miFuncion(0,"C") devolverá 32°F.*/

const convertirGrados = (grados = undefined, unidad = "") => {
  if (grados === undefined)
    return console.warn("No ingresaste los grados a convertir");

  if (typeof grados !== "number")
    return console.error(`El valor "${grados}" ingresado no es un número`);

  if (!unidad) return console.warn("No ingresaste la unidad de conversión");

  if (typeof unidad !== "string")
    return console.error(`El valor "${unidad}" ingresado no es una cadena de texto`);

  if (unidad.length !== 1 || !/C|F/.test(unidad))
    return console.warn("valor de unidad no reconocido");

  return (unidad === "C")
    ? console.info(`${grados}°C = ${Math.round(grados * (9 / 5) + 32)}°F`)
    : unidad === "F"
    ? console.info(`${grados}°F son ${Math.round((grados - 32) * (5 / 9))}°C`)
    : console.warn("unidad de conversión erronea");
};

// convertirGrados();
// convertirGrados("2");
// convertirGrados(2);
// convertirGrados(2,true);
// convertirGrados(2,"Hola");
// convertirGrados(2,"E");
// convertirGrados(0,"C");
// convertirGrados(100,"C");
// convertirGrados(32,"F");
// convertirGrados(100,"F");
