/* 15) Programa una función para convertir números de base binaria a decimal y viceversa, pe. miFuncion(100,2) devolverá 4 base 10. */

const convertirBaseDecimal = (numero = undefined, base = undefined) => {
  if (numero === undefined) return console.warn("No ingresaste un número");

  if (typeof numero !== "number")
    return console.error(`El valor "${numero}" ingresado, no es un número`);

  if (!base) return console.warn("No ingresaste la base a convertir");

  if (typeof base !== "number")
    return console.error(`El valor "${base}" ingresado, no es un número`);

  if (base === 2) {
    //El primer parametro de parseInt es el número a convertir a entero
    //El segundo parametro de parseInt es la base que queremos convertir a decimal
    return console.info(`${numero} base 2 = ${parseInt(numero, base)} base 10`);
  } else if (base === 10) {
    //El parametro de toString es la base a la que vamso a convertir el número
    return console.info(`${numero} base 10 = ${parseInt(numero, base)} base 2`);
  } else {
    return console.error("El tipo de base a convertir, no es válida");
  }
};

// convertirBaseDecimal();
// convertirBaseDecimal("100");
// convertirBaseDecimal(100,);
// convertirBaseDecimal(100,"2");
// convertirBaseDecimal(100,2);
// convertirBaseDecimal(4,10);

/* 16) Programa una función que devuelva el monto final después de aplicar un descuento a una cantidad dada, pe. miFuncion(1000, 20) devolverá 800. */

const aplicarDescuento = (monto = undefined, descuento = 0) => {
  if (monto === undefined) return console.warn("No ingresaste el monto");

  if (typeof monto !== "number")
    return console.error(`El valor "${descuento}" ingresado, no es un número`);

  if (monto === 0) return console.error("El monto no puede ser 0");

  if (Math.sign(monto) === -1)
    return console.error("El monto no puede ser negativo");

  if (typeof descuento !== "number")
    return console.error(`El valor "${descuento}" ingresado, no es un número`);

  if (Math.sign(descuento) === -1)
    return console.error("El descuento no puede ser negativo");

  return console.info(
    `$${monto} - ${descuento}% = $${monto - (descuento * monto) / 100}`
  );
};

// aplicarDescuento()
// aplicarDescuento("1000")
// aplicarDescuento(0)
// aplicarDescuento(-1000)
// aplicarDescuento(1000, "20")
// aplicarDescuento(1000, 20)
// aplicarDescuento(1000)
// aplicarDescuento(1000, 25)

/* 17) Programa una función que dada una fecha válida determine cuantos años han pasado hasta el día de hoy, pe. miFuncion(new Date(1984,4,23)) devolverá 35 años (en 2020). */

const calcularAnios = (fecha = undefined) => {
  if (fecha === undefined) return console.warn("No ingresaste una fecha");

  if (!(fecha instanceof Date))
    return console.error("El valor ingresado no es una fecha válida");

  let hoyMenosFecha = new Date().getTime() - fecha.getTime(),
    aniosEnMs = 1000 * 60 * 60 * 24 * 365,
    aniosHumanos = Math.floor(hoyMenosFecha / aniosEnMs);

  return Math.sign(aniosHumanos) === -1
    ? console.info(`Faltan ${Math.abs(aniosHumanos)} años para el ${fecha.getFullYear()}`)
    : Math.sign(aniosHumanos) === 1
    ? console.info(`Han pasado ${Math.abs(aniosHumanos)} años desde el ${fecha.getFullYear()}`)
    : console.info(`Estamos en el año actual ${fecha.getFullYear()}`);
};

// calcularAnios();
// calcularAnios({});
// calcularAnios(false);
// calcularAnios(new Date());
// calcularAnios(new Date(2001,5,21));
// calcularAnios(new Date(1984,4,23));
// calcularAnios(new Date(1884,5,21));
