/* 18) Programa una función que dada una cadena de texto cuente el número de vocales y consonantes, pe. miFuncion("Hola Mundo") devuelva Vocales: 4, Consonantes: 5. */

const contarVC = (cadena = "") => {
  if (!cadena) return console.warn("No ingresaste una cadena de texto");

  if (typeof cadena !== "string")
    return console.error(`El valor "${cadena}" ingresado, no es una cadena de texto`);

  cadena = cadena.toLowerCase();

  let vocales = 0,
    consonantes = 0;

  for (const letra of cadena) {
    if (/[aeiouáéíóúü]/.test(letra)) vocales++;

    if (/[bcdfghjklmnñpqrstvwxyz]/.test(letra)) consonantes++;
  }

  return console.info({
    cadena,
    vocales,
    consonantes,
  });
};

// contarVC();
// contarVC(3);
// contarVC("Hola Mundo");
// contarVC("Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur fugit repudiandae blanditiis tempore dolorum quos tempora at incidunt vero nisi minima, assumenda suscipit, voluptatem aliquam illum, repellat quod? Tempore, voluptatum");

/* 19) Programa una función que valide que un texto sea un nombre válido, pe. miFuncion("Jonathan MirCha") devolverá verdadero. */

const validadrNombre = (nombre = "") => {
  if (!nombre) return console.warn("No ingresaste un nombre");

  if (typeof nombre !== "string")
    return console.error(`El valor "${nombre}" ingresado, no es una cadena de texto`);

  let expReg = /^[A-Za-zÑñÁáÉéÍíÓóúÚÜü\s]+$/g.test(nombre);

  return (expReg)
    ? console.info(`"${nombre}", es un nombre válido`)
    : console.info(`"${nombre}", no es un nombre válido`);
};

// validadrNombre();
// validadrNombre(12312);
// validadrNombre("Alejandro");
// validadrNombre("José Alejandro");
// validadrNombre("José Alejandro, 35");

/* 20) Programa una función que valide que un texto sea un email válido, pe. miFuncion("jonmircha@gmail.com") devolverá verdadero. */

const validarEmail = (email) => {
  if (!email) return console.warn("No ingresaste un email");

  if (typeof email !== "string")
    return console.error(`El valor "${email}" ingresado, no es una cadena de texto`);

  let expReg2 = /[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/i.test(email);

  return expReg2
    ? console.info(`"${email}", es un email válido`)
    : console.info(`"${email}", no es un email válido`);
};

// validarEmail();
// validarEmail(34);
// validarEmail("joseale462@gmail.");
// validarEmail("istrejo2106@gmail.com");

/* Fusión 19-20 */

const validarPatron = (cadena = "", patron = undefined) => {
  if (!cadena)
    return console.warn("No ingresaste una cadena de texto a evaluar");

  if (typeof cadena !== "string")
    return console.error(`El valor "${cadena}" ingresado, no es una cadena de texto`);

  if (patron === undefined)
    return console.warn("No ingresaste un patrón a evaluar");

  if (!(patron instanceof RegExp))
    return console.error(`El valor "${patron}" ingresado, no es una expresión regular`);

  let expReg = patron.test(cadena);

  return expReg
    ? console.info(`"${cadena}", cumple con el patrón ingresado`)
    : console.warn(`"${cadena}", no cumple con el patrón ingresado`);
};

validarPatron();
validarPatron({});
validarPatron("Hola Mundo");
validarPatron("Hola Mundo", "hola");
validarPatron("Hola Mundo", [1, 2, 3]);
validarPatron("José Alejandro", /^[A-Za-zÑñÁáÉéÍíÓóúÚÜü\s]+$/g);
validarPatron("José Alejandro 19", /^[A-Za-zÑñÁáÉéÍíÓóúÚÜü\s]+$/g);
// validarPatron("joseale462@gmail",new RegExp("/[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/", "i") );
// validarPatron("joseale462@gmail.com",new RegExp("/[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/", "i") );
validarPatron("joseale462@gmail",new RegExp(/[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/,"i"));
validarPatron("joseale462@gmail.com",new RegExp(/[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/,"i")
);

//para poder hacerlo con el constructor new RegExp, tuve que hacerlo de esta forma: new RegExp(/[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})/, "i") );. Cuando encerraba la expresión con comillas dobles siempre decía que no cumplía con el patrón dado. ¿Cambio la forma de escribir la expresiones con el constructor?
