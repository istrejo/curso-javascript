/* 21) Programa una función que dado un array numérico devuelve otro array con los números elevados al cuadrado, pe. mi_funcion([1, 4, 5]) devolverá [1, 16, 25]. */

const arrayAlCuadrado = (arr = undefined) => {
  if (arr === undefined)
    return console.warn("No ingresaste un arreglo de números");

  if (!(arr instanceof Array))
    return console.error(`El valor "${arr}" ingresado, no es un array`);

  if (arr.length === 0) return console.error("El arreglo está vacío");

  for (let num of arr) {
    if (typeof num !== "number")
      return console.error(`El elemento "${num}" del arreglo no es un número`);
  }

  const alCuadrado = arr.map((elem) => elem * elem);

  return console.info(
    `Arreglo original: ${arr}.\nArreglo elevado al cuadrado: ${alCuadrado}`
  );
};

// arrayAlCuadrado();
// arrayAlCuadrado(true);
// arrayAlCuadrado({});
// arrayAlCuadrado([]);
// arrayAlCuadrado([1,"3",4,{}]);
// arrayAlCuadrado([1,4,{}]);
// arrayAlCuadrado([1, 4, 5])

/* 22) Programa una función que dado un array devuelva el número mas alto y el más bajo de dicho array, pe. miFuncion([1, 4, 5, 99, -60]) devolverá [99, -60]. */

const arrayMinMax = (arr) => {
  if (arr === undefined)
    return console.warn("No ingresaste el arreglo a evaluar");

  if (!(arr instanceof Array))
    return console.error(`El valor "${arr}" ingresado, no es un array`);

  if (arr.length === 0) return console.error("El arreglo esta vacío");

  for (const num of arr) {
    if (typeof num !== "number")
      return console.error(`El elemento "${num}" del arreglo no es un número`);
  }

  return console.info(
    `Arreglo original: ${arr}\nValor mayor: ${Math.max(
      ...arr
    )}\nValor menor: ${Math.min(...arr)};`
  );
};

arrayMinMax();
arrayMinMax(false);
arrayMinMax([]);
arrayMinMax([2, 3, true]);
arrayMinMax([1, 4, 5, 99, -60]);

/* 23) Programa una función que dado un array de números devuelva un objeto con 2 arreglos en el primero almacena los números pares y en el segundo los impares, pe. miFuncion([1,2,3,4,5,6,7,8,9,0]) devolverá {pares: [2,4,6,8,0], impares: [1,3,5,7,9]}. */

const separarParesImpares = (arr) => {
  if (arr === undefined)
    return console.warn("No ingresaste el arreglo a evaluar");

  if (!(arr instanceof Array))
    return console.error(`El valor "${arr}" ingresado, no es un array`);

  if (arr.length === 0) return console.error("El arreglo esta vacío");

  for (const num of arr) {
    if (typeof num !== "number")
      return console.error(`El elemento "${num}" del arreglo no es un número`);
  }

  return console.info({
    arr,
    pares: arr.filter((elem) => elem % 2 === 0),
    impares: arr.filter((elem) => elem % 2 !== 0),
  });
};

separarParesImpares([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);
